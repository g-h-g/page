/*
We use constants in Vuex for several reasons:
1. Preventing duplicate names
2. Preventing usage/calling of getter/mutation/action with non-existent name
3. Getting advantage of IDE code-completion and usage of constants (i.e. to spot unused names)
*/

// getters
export const GET_FIRST_NAME = 'GET_FIRST_NAME';
export const GET_LAST_NAME = 'GET_LAST_NAME';
export const GET_PHONE = 'GET_PHONE';
export const GET_EMAIL = 'GET_EMAIL';
export const GET_STATS = 'GET_STATS';
export const GET_TOKEN = 'GET_TOKEN';
export const HAS_AUTH = 'HAS_AUTH';

// mutations
export const SET_FIRST_NAME = 'SET_FIRST_NAME';
export const SET_LAST_NAME = 'SET_LAST_NAME';
export const SET_PHONE = 'SET_PHONE';
export const SET_EMAIL = 'SET_EMAIL';
export const SET_TOKEN = 'SET_TOKEN';
export const SET_DATE_CREATED = 'SET_DATE_CREATED';
export const SET_DATE_MODIFIED = 'SET_DATE_MODIFIED';
export const SET_UUID = 'SET_UUID';
export const SET_PLATFORM = 'SET_PLATFORM';
