
const redirects = [
  {
    from: '/de/K%C3%A4ltetechniker',
    to: '/kaeltetechniker'
  },
  {
    from: '/de/Projektleiter',
    to: '/projektleiter'
  },
  {
    from: '/de/Servicetechniker',
    to: '/servicetechniker'
  },
  {
    from: '/de/kaeltetechniker',
    to: '/kaeltetechniker'
  },
  {
    from: '/de/projektleiter',
    to: '/projektleiter'
  },
  {
    from: '/de/servicetechniker',
    to: '/servicetechniker'
  },
  {
    from: '/K%C3%A4ltetechniker',
    to: '/kaeltetechniker'
  }
];

const routes = [
  ...redirects.map(entry => ({
    path: entry.from,
    redirect: entry.to
  })),

  {
    path: '/',
    name: 'home',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        name: 'index',
        component: () => import('pages/Index.vue')
      },
      {
        path: 'success',
        name: 'submitSuccessful',
        component: () => import('pages/SubmitSuccessful.vue')
      },
      {
        path: 'impressum',
        name: 'imprint',
        component: () => import('pages/PageImprint.vue')
      },
      {
        path: 'auth',
        name: 'auth',
        component: () => import('pages/Auth.vue'),
      },
      {
        path: 'register',
        name: 'register',
        component: () => import('pages/Auth.vue'),
      },
      {
        path: 'auth/admin',
        name: 'admin-auth',
        component: () => import('pages/Auth.vue'),
      },
      {
        // must be after /success - otherwise :jobid will match "success"
        path: ':title?',
        name: 'job',
        props: true,
        component: () => import('pages/PageApply.vue')
      },
      {
        // must be after /success - otherwise :jobid will match "success"
        path: ':title?/:date?(20\\d\\d-\\d\\d-\\d\\d)',
        name: 'job-date',
        props: true,
        component: () => import('pages/PageApply.vue')
      },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
];

export default routes;
