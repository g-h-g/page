export default
{
  localeName: 'Deutsch',
  previewForm: 'Vorschau',
  abortForm: 'Abbrechen',
  noteAbort: 'alle Formular Daten werden gelöscht.',
  submitting: 'Bewerbung absenden',
  submitFailed: 'Versand fehlgeschlagen',
  success: 'Success',
  applicationWasSent: 'Bewerbung wurde versendet.',
  goHome: 'Gehe zur Homepage',
  fileNotFound: 'Seite nicht gefunden',
  Imprint: 'Impressum',
  Privacy: 'Datenschutz',
  more_jobs: 'Weitere Stellenangebote',
  files:
  {
    photoRejected: 'Das Foto ist größer als 9 MB',
    attachmentsRejected: '{count} Dateien wurden abgelehnt, weil sie insgesamt größer als 9 MB sind'
  },
  msg: {
    job_saved_success: 'Stellenanzeige wurde erfolgreich gespeichert',
    login_successfull: 'Erfolgreich eingeloggt!',
    login_failed: 'Anmeldung fehlgeschlagen.',
    forgot_password_failed: 'Versand der Mail zum Zurücksetzen des Kennwortes fehlgeschlagen. {0}',
    registration_successfull: 'Die Registrierung war erfolgreich. Bitte prüfen Sie ihre Mails!'
  },
  nav:
  {
    applications: 'Bewerbungen',
    application: 'Bewerber',
    attachments: 'Anlagen',
    dashboard: 'Dashboard',
    demo: 'Demo',
    settings: 'Einstellungen',
    jobs: 'Stellenanzeigen',
    job_title: 'Job title',
    project: 'Projekt',
    projects: 'Projekte',
    organizations: 'Firmen',
    misc: 'Sonstiges',
    statistics: 'Statistiken',
    clicks: 'Klicks',
    state: 'Status',
    organization: 'Firma',
    create_org: 'Neue Firma anlegen',
    update_org: 'Unternehmen aktualisieren',
    contact: 'Kontakt',
    edit_org: 'Firma bearbeiten',
    del_org: 'Firma löschen',
    edit_job: 'Anzeige bearbeiten',
    del_job: 'Anzeige löschen',
    edit_application: 'Bewerbung bearbeiten',
    del_application: 'Bewerbung löschen',
  },
  btn: {
    login: 'Anmelden',
    logout: 'Abmelden',
    close: 'Schliessen',
    email: 'E-Mail',
    preview: 'Vorschau',
    cancel: 'Abbrechen',
    abort: 'Abbrechen',
    back: 'Zurück',
    continue: 'Weiter',
    download: 'Download',
    register: 'Registrieren',
    save: 'Speichern',
    send: 'Senden',
    publish: 'Veröffentlichen',
    apply_postmail: 'Bewerbung per Post',
    apply_text: 'jetzt bewerben',
    views: 'Seiten',
    visits: 'Besucher',
    clicks: 'Klicks',
  },
  label:
  {
    username: 'Benutzername',
    subject: 'Betreff',
    company: 'Firmenname',
    organization: 'Firmenname',
    country: 'Land',
    countries: 'Länder',
    location: 'Ort',
    email_or_username: 'Email oder Benutzername',
    forgot_password: 'Kennwort vergessen',
    reset_password: 'Kennwort zurücksetzen',
    password: 'Kennwort',
    password_repeat: 'Kennwort wiederholen',
    reference: 'Referenz',
    freelance: 'Freie Mitarbeit',
    contract: 'Festanstellung',
    internship: 'Praktikum',
    apprenticeship: 'Ausbildungsplatz',
    fulltime: 'Vollzeit',
    parttime: 'Teilzeit',
    minijob: 'Minijob',
    shiftwork: 'Schichtarbeit',
    work_duration: 'Pensum',
    work_kind: 'Art der Anstellung',
    visibility: 'Sichtbarkeit',
    salary: 'Jahresgehalt',
    homeoffice: 'Homeoffice',
  },
  buttons:
  {
    apply: 'Bewerben',
    yes: 'Ja',
    no: 'Nein',
    finish: 'Beenden',
    continue: 'Weiter',
    back: 'Zurück',
    send: 'Absenden',
    cancel: 'Abbrechen',
    close: 'Schließen',
    remove: 'Löschen',
    preview: 'Vorschau',
    contact: 'Kontakt',
  },
  rules:
  {
    required: 'Pflichtfeld',
    phoneOrEmail: 'Bitte geben sie entweder eine Telefonnummer oder eine E-Mail Adresse an',
    invalidDate: 'ungültiges Datum',
    invalidEmail: 'ungültige E-Mail Adresse'
  },
  stepOne:
  {
    title: 'Persönliche Angaben',
    salutation: 'Anrede',
    salutationTypes:
    {
      Mister: 'Herr',
      Missis: 'Frau',
    },
    firstName: 'Vorname',
    lastName: 'Name',
    street: 'Strasse',
    houseNumber: 'Hausnummer',
    zipCode: 'PLZ',
    city: 'Ort',
    country: 'Land',
    phone: 'Mobilnummer',
    email: 'E-Mail',
    date: 'Wann bist du erreichbar? z.B. Montag morgens.',
    social:
    {
      title: 'Soziale Profile',
      import: 'Daten aus sozialen Profilen importieren',
    }
  },
  stepTwo:
  {
    title: 'Anschreiben',
    description: 'Sie können etwas über sich schreiben, von dem Sie glauben, dass es dem Personalverantwortlichen helfen wird, Sie für die Stelle auszuwählen.'
  },
  stepThree:
  {
    title: 'Fragen',
    startDate: 'frühester Eintrittstermin?',
    immediately: 'sofort',
    expectedSalary: 'Wie hoch ist ihre Gehaltsvorstellung?',
  },
  stepFour:
  {
    title: 'Anhänge',
    help: 'Bilder oder PDF-, DOC(x)-, XLS(x)-Dokumente anhängen - bis zu 9 MB',
    photo: 'Sie können ein Bewerbungsfoto hochladen',
    status: 'Status: {status}',
  },
  stepFive:
  {
    title: 'Datenschutz',
    ccTitle: 'Kopie meiner Daten',
    carbonCopy: 'Senden sie mir eine Kopie meiner Datein an meine E-Mail.',
    privacyPolicy: 'Hinweis zum Datenschutz: Nach dem Absenden der eingegebenen Daten werden diese auf unserem Server verarbeitet und in einer Mail an die für die Bearbeitung zuständige Person weitergeleitet. Wir sichern Ihnen dabei ein Höchstmaß an Vertraulichkeit zu und versichern die Einhaltung aller gesetzlichen Vorschriften. Weitere Informationen zur Datenverarbeitung erhalten Sie auf unserer <a href="{0}" target="_blank" rel="noopener noreferrer" onclick="event.stopPropagation(); return true;">Datenschutzbestimmungen</a>.' // the link won't work without the onClick handler
  },
  dropZone:
  {
    dragDrop: 'Sie können Dokumente hier ablegen - oder ',
    clickHere: 'klicken Sie hier',
    chooseManually: ', um Dateien manuell zu wählen.'
  },
  preview:
  {
    title: 'Vorschau',
    email: 'E-Mail',
    phone: 'Tel.',
    contact: 'Kontakt',
    address: 'Anschrift',
    canStart: 'frühester Eintrittstermin',
    expectedSalary: 'Gehaltsvorstellung',
  },
  salary:
    {
      periodTitle: 'Zeitraum',
      period:
        {
          1: 'Jahr',
          2: 'Monat',
          3: 'Stunde',
        },
      amount: 'Gehaltsvorstellung',
      currency: 'Währung',
    },
  copyright: '\xA9 {year} GHG',
  login: 'Anmelden',
  pageTitleApplication: 'Bewerbung auf: {title}',
  speculativeApplication: 'Initiativbewerbung',
  yourLogo: 'GHG Haustechnik!',
  logout: 'Abmelden',
};
