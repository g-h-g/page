'use strict';

import JobsData from 'src/data/jobs.json';

function list()
{
  return JobsData;
}

function detail(id)
{
  return JobsData[id];
}

function reference(title)
{
  const words = title.split(' ');
  return words[0];
}

export default {
  list,
  detail,
  reference
};
