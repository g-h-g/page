module.exports = {
  apps: [{
    name: '@ghg/frontend',
    script: 'yarn',
    interpreter: '/bin/bash',
    args: 'start',
    env: {
      NODE_ENV: 'production',
      PORT: 5001
    },
  }],

  deploy: {
    production: {
      user: 'yawik',
      host: 'ghgnetz.de',
      ref: 'origin/main',
      repo: 'https://gitlab.com/g-h-g/page.git',
      path: '/home/yawik/www.ghgnetz.de',
      'pre-deploy-local': 'rsync -a --exclude=node_modules --delete dist/spa/ yawik@ghgnetz.de:/home/yawik/www.ghgnetz.de/source/packages/frontend/dist/spa/',
      'post-deploy': 'cd /home/yawik/www.ghgnetz.de/source/packages/frontend/ && pm2 startOrRestart ecosystem.config.js --interpreter bash --env production',
      'pre-setup': 'pm2 ps'
    }
  }
};
