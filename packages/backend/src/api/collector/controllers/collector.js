'use strict';

/**
 *  collector controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::collector.collector', ({strapi}) => ({
    async create(ctx) {
        const jobId = ctx.request.body.data.job;
        const today = new Date().toISOString().substring(0, 10);
        const exitscollector = await strapi.service('api::collector.collector').find({
            filters: { 
                job: jobId,
                date: today
            }
        });
        if (exitscollector && exitscollector.results && exitscollector.results.length > 0) {
            const collectorBody = exitscollector.results[0];
            collectorBody.views += ctx.request.body.data.views;
            collectorBody.clicks += ctx.request.body.data.clicks;
            collectorBody.date = today; 
            const collector = await strapi.service('api::collector.collector').update(collectorBody.id, {data: collectorBody});
            return collector;
        } else {
            const data = ctx.request.body;
            data.data.date = today;
            const collector = await strapi.service('api::collector.collector').create(data);
            return collector;
        }
        
    }
}));
