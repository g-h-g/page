module.exports = ({ env }) => ({
  email: {
    config: {
      provider: 'nodemailer',
      providerOptions: {
        host: env('EMAIL_SMTP_HOST', 'mc.cross-solution.de'), //SMTP Host
        port: env('EMAIL_SMTP_PORT', 465), //SMTP Port
        secure: env('EMAIL_SMTP_SECURE', true),
        auth: {
          user: env('EMAIL_SMTP_USERNAME', ''),
          pass: env('EMAIL_SMTP_PASSWORD', ''),
        },
        rejectUnauthorized: true,
        requireTLS: true,
      },
      settings: {
        defaultFrom: env('EMAIL_SETTINGS_FROM', ''),
        defaultReplyTo: env('EMAIL_SETTINGS_REPLYTO', '')
      },
      
    },
  }
});
