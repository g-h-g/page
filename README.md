# GHG Jobs

lerna managed multirepo containing a strapi backend and a quasar frontend

## Requirements

- yarn
- node >=16

## Installation

```
git clone https://gitlab.com/g-h-g/page.git
cd page
yarn && yarn lerna bootstrap
```
## Usage

Backend can be started by:

```
yarn start:backend
```

Point your browser at: http://127.0.0.1:1337


Frontend can be started by:

```
yarn start:frontend
```

Point your browser at: http://127.0.0.1:8080

